import { Component, OnInit } from '@angular/core';
import { NbDialogRef, NbDialogService } from '@nebular/theme';

//imports para la tabla
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';

export interface listAgencias {
  agencia: string;
  distrito: string;
  provincia: string;
  departamento: string;

}

const ELEMENT_DATA: listAgencias[]=[
  {
    agencia: "Las Flores",
    distrito: "San Juan De Lurigancho",
    provincia: "Lima",
    departamento: "Lima",
  },
  {
    agencia: "Punchana",
    distrito: "Punchana",
    provincia: "Maynas",
    departamento: "Loreto",

  },
  {
    agencia: "Conquistadores",
    distrito: "San Isidro",
    provincia: "Lima",
    departamento: "Lima",
  },
 ];

@Component({
  selector: 'app-list-agencias',
  templateUrl: './list-agencias.component.html',
  styleUrls: ['./list-agencias.component.css']
})

export class ListAgenciasComponent implements OnInit {

  constructor(protected dialogRef: NbDialogRef<any>) { }

  ngOnInit() {
  }

  selectedItem = '2';

  displayedColumns: string[] = ['agencia', 'distrito', 'provincia', 'departamento'];
  dataSource = new MatTableDataSource<listAgencias>(ELEMENT_DATA);
  selection = new SelectionModel<listAgencias>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */

  /** The label for the checkbox on the passed row */
/*   checkboxLabel(row?: listAgencias): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  } */

}
