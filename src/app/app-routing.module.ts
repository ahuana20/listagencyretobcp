import { NgModule } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import { ListAgenciasComponent } from './list-agencias/list-agencias.component';

const routes: Routes = [
  { path: '', redirectTo: '/agenci', pathMatch: 'full' },
  { path: 'agenci/:id', component:  ListAgenciasComponent},

];


/* {
  path: 'pages',
  loadChildren: () =>
    import('./pages/pages.module').then((m) => m.PagesModule),
  canActivate: [AuthGuard],
}, */

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [ListAgenciasComponent]
